﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using Tulpep.NotificationWindow;

namespace Jenny
{

	public class Client
	{
		public static readonly string HOTELEMERGENCY = "h0"
		, HOTELGUESTDATA = "h1"
		, HOTELPLACERECOMMENDATION = "h2"
		, HOTELRESTURANTRECOMMENDATION = "h3"
		, HOTELSOUNDS = "h4"
		, HOTELTRANSPORTRECOMMENDATION = "h5"
		, HOTELWIFI = "h6"
		, HOTELDATA = "h7";
		private MqttClient mqttclient;
		private static Dictionary<string, string> HotelInfo;
		public Client(string ipaddress)
		{
			//MqttClient client = new MqttClient("jennylocal");
			mqttclient = new MqttClient(ipaddress);
			mqttclient.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
			mqttclient.Connect("hotel");
			mqttclient.Subscribe(new string[] { "hotel" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });

			StreamReader r = new System.IO.StreamReader("hotelInfo.json");
			var json = r.ReadToEnd();
			HotelInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
			r.Close();
		}
		void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
		{
			var json = Encoding.UTF8.GetString(e.Message);
			var msg = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
			Console.WriteLine("recieved: "+Encoding.UTF8.GetString(e.Message));
			string error = ""; MySqlDataReader returned_reader = null;
			if (msg["dtype"] == HOTELEMERGENCY)
			{
				DBConnect dbconnect = new DBConnect(HotelInfo["DBUser"], HotelInfo["DBName"], HotelInfo["DBPassword"]);
				bool result = dbconnect.GetRoom_no(ref error, ref returned_reader, Int32.Parse(msg["source_id"]));
				if (result)
				{
					returned_reader.Read();
					string room_no = returned_reader[1].ToString();
					Console.WriteLine("room" + room_no);
					string emergency_msg = "Room# " + room_no + " need to help!";
					Console.WriteLine(emergency_msg);
					send_to_assistant(msg["source_id"], "ACK", HOTELEMERGENCY);
                    //notifier
                    PopupNotifier popup = new PopupNotifier();
                    popup.TitleText = "Emergency";
                    popup.ContentText = "help room number whatever!";
                    popup.Popup();
                    DateTime localDate = DateTime.Now;
                    StreamReader sr = new System.IO.StreamReader("emergency.json");
                    var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(sr.ReadToEnd());

                    sr.Close();
                    Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();
                    dict.Add("room", "8");
                    dict.Add("datetime", localDate);
                    dict.Add("notification content", "blabla");
                    list.Add(dict);

                    //dataGridView2.DataSource = list;
                    //dataGridView2.Update();
                    //dataGridView2.Refresh();
                    var ll = Newtonsoft.Json.JsonConvert.SerializeObject(list);

                    StreamWriter sw = new StreamWriter("emergency.json");
                    sw.Write(ll);
                    sw.Close();
                }
			}
			else if (msg["dtype"] == HOTELGUESTDATA)
			{
				DBConnect dbconnect = new DBConnect( HotelInfo["DBUser"], HotelInfo["DBName"], HotelInfo["DBPassword"]);
				bool result = dbconnect.GetRoom_no(ref error, ref returned_reader, Int32.Parse(msg["source_id"]));
				if (result)
				{
					returned_reader.Read();
					string room_no = returned_reader[1].ToString();
					Console.WriteLine(room_no);
					returned_reader.Close();
					send_guest_data(msg["source_id"], room_no);
				}
			}
			else if (msg["dtype"] == HOTELSOUNDS)
			{
				DBConnect dbconnect = new DBConnect( HotelInfo["DBUser"], HotelInfo["DBName"], HotelInfo["DBPassword"]);
				bool result = dbconnect.RetrieveTableData(ref error, ref returned_reader, "sounds");
				Dictionary<int, string> reply = new Dictionary<int, string>();
				int index = 0;
				if (result)
				{
					while (returned_reader.Read())
					{
						reply.Add(index, returned_reader[1].ToString() + " by " + returned_reader[2].ToString() + " played in " + returned_reader[3].ToString());
						index++;
					}

				}
				var entries = reply.Select(d =>
				string.Format("\"{0}\": \"{1}\"", d.Key, string.Join(",", d.Value)));
				string msg_json = "{" + string.Join(",", entries) + "}";
				send_to_assistant(msg["source_id"], msg_json, HOTELSOUNDS);
			}
			else if (msg["dtype"] == HOTELWIFI)
			{
				DBConnect dbconnect = new DBConnect( HotelInfo["DBUser"], HotelInfo["DBName"], HotelInfo["DBPassword"]);
				bool result = dbconnect.GetRoom_no(ref error, ref returned_reader, Int32.Parse(msg["source_id"]));
				if (result)
				{
					returned_reader.Read();
					string room_no = returned_reader[1].ToString();
					Console.WriteLine("room" + room_no);
					returned_reader.Close();
					MySqlDataReader returned_wifi_reader = null;
					result = dbconnect.GetWifiPassword(ref error, ref returned_wifi_reader, room_no);
					if (result)
					{
						returned_wifi_reader.Read();
						string wifi_password = returned_wifi_reader[1].ToString();
						returned_wifi_reader.Close();
						Console.WriteLine("wifi" + wifi_password);
						send_to_assistant(msg["source_id"], wifi_password, HOTELWIFI);
					}
				}
			}
			else if (msg["dtype"] == Client.HOTELDATA)
			{
				Console.WriteLine(msg["data"]);
				StreamReader r = new System.IO.StreamReader("hotelInfo.json");
				var data = r.ReadToEnd();
				r.Close();
				Dictionary<string, string> Hotel_data = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
				Dictionary<string, string> reply = new Dictionary<string, string>();
				reply.Add("name", Hotel_data["HotelName"]);
				reply.Add("country", Hotel_data["Country"]);
				reply.Add("city", Hotel_data["City"]);
				reply.Add("latitude", Hotel_data["Latitude"]);
				reply.Add("longitude", Hotel_data["Longitude"]);
				
				var entries = reply.Select(d =>
				string.Format("\"{0}\": \"{1}\"", d.Key, string.Join(",", d.Value)));
				string msg_json = "{" + string.Join(",", entries) + "}";
				Console.WriteLine(msg_json.ToString());
				if (msg["source_type"] == "local")
				{
					send_to_local_server(msg_json, HOTELDATA);
				}
				else
				{
					send_to_assistant(msg["source_id"], msg_json, HOTELDATA);
				}
			}
		}
		public void send_guest_data(string room_no)
		{
			string error = ""; MySqlDataReader returned_reader = null;
			DBConnect dbconnect = new DBConnect( HotelInfo["DBUser"], HotelInfo["DBName"], HotelInfo["DBPassword"]);
			bool result = dbconnect.GetAssistant_id(ref error, ref returned_reader, room_no);
			if (result)
			{
				returned_reader.Read();
				string assistant_id = returned_reader[0].ToString();
				Console.WriteLine(room_no);
				returned_reader.Close();
				send_guest_data(assistant_id, room_no);
			}
		}
		private void send_guest_data(string assistant_id, string room_no)
		{
			string error = ""; MySqlDataReader returned_reader = null;
			DBConnect dbconnect = new DBConnect( HotelInfo["DBUser"], HotelInfo["DBName"], HotelInfo["DBPassword"]);
			bool result = dbconnect.GetGuestData(ref error, ref returned_reader, room_no);
			Dictionary<string, string> reply = new Dictionary<string, string>();
			if (result)
			{
				returned_reader.Read();
				reply.Add("name", returned_reader[1].ToString());
				//reply.Add("phone", returned_reader[2].ToString());
				reply.Add("mail", returned_reader[3].ToString());
				//reply.Add("DOB", returned_reader[4].ToString());
				//reply.Add("check_in_date", returned_reader[5].ToString());
				//reply.Add("check_out_date", returned_reader[6].ToString());
				returned_reader.Close();
			}
			var entries = reply.Select(d =>
			string.Format("\"{0}\": \"{1}\"", d.Key, string.Join(",", d.Value)));
			string msg_json = "{" + string.Join(",", entries) + "}";
			Console.WriteLine(msg_json.ToString());
			send_to_assistant(assistant_id, msg_json, HOTELGUESTDATA);
		}
		private void send_to_local_server(string data, string data_type)
		{
			Dictionary<string, string> msg = new Dictionary<string, string>();
			msg.Add("dtype", data_type);
			if (data.IndexOf(@"{") != -1)
			{
				data = "[" + data + "]";
			}
			msg.Add("data", data);
			msg.Add("source_type", "hotel");
			msg.Add("source_id", null);
			msg.Add("destination_type", "local");
			msg.Add("destination_id", null);
			var entries = msg.Select(d =>
			string.Format("\"{0}\": \"{1}\"", d.Key, string.Join(",", d.Value)));
			string msg_json = "{" + string.Join(",", entries) + "}";
			if (data.IndexOf(@"{") != -1)
			{
				msg_json = msg_json.Remove(msg_json.IndexOf(@"[") - 1, 1);
				msg_json = msg_json.Remove(msg_json.IndexOf(@"]") + 1, 1);
			}
			Console.WriteLine(msg_json);
			mqttclient.Publish("local", Encoding.UTF8.GetBytes(msg_json), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
		}
		private void send_to_assistant(string assistant_id, string data, string data_type)
		{
			Dictionary<string, string> msg = new Dictionary<string, string>();
			msg.Add("dtype", data_type);
			if (data.IndexOf(@"{") != -1)
			{
				data= "[" + data + "]";
			}
			msg.Add("data", data);
			msg.Add("source_type", "hotel");
			msg.Add("source_id", null);
			msg.Add("destination_type", "assistant");
			msg.Add("destination_id", assistant_id);
			var entries = msg.Select(d =>
			string.Format("\"{0}\": \"{1}\"", d.Key, string.Join(",", d.Value)));
			string msg_json = "{" + string.Join(",", entries) + "}";
			if (data.IndexOf(@"{") != -1)
			{
				msg_json = msg_json.Remove(msg_json.IndexOf(@"[") - 1, 1);
				msg_json = msg_json.Remove(msg_json.IndexOf(@"]") + 1, 1);
			}
			Console.WriteLine(msg_json);
			mqttclient.Publish("local", Encoding.UTF8.GetBytes(msg_json), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
		}
	}
}
