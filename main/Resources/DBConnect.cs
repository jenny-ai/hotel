using System;
using MySql.Data.MySqlClient;
namespace Jenny
{
	class DBConnect
	{
		private MySqlConnection connection;
		private string server;
		private string database;
		private string uid;
		private string password;
		public DBConnect(string database, string uid, string password)
		{
        	this.server = "localhost";
			this.database = database;
			this.uid = uid;
			this.password = password;
			string connectionString;
			connectionString = "SERVER=" + "localhost" + ";" + "DATABASE=" +
			database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

			this.connection = new MySqlConnection(connectionString);
			try
			{
				connection.Open();
				Console.WriteLine("Connection Open !");
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error: {0}", ex.ToString());
			}
		}


		
		public bool AddNewGuest(ref string error, string room_no, string name, string mail, string phone_no,string ID_no, string credit_no, DateTime DOB, DateTime check_in_date, DateTime check_out_date)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				cmd.CommandText = "INSERT INTO guests(room_no, name, phone_no, mail, ID_no, credit_no, DOB, check_in_date, check_out_date)" +
								 " VALUES('" + room_no + "', '"
								 + name + "', '"
								 + phone_no + "', '"
								 + mail + "', '"
								 + ID_no + "', '"
								 + credit_no + "', '"
								 + DOB.ToString("d") + "', '"
								 + check_in_date.ToString("d") + "', '"
								 + check_out_date.ToString("d") + "')";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool GetGuestData(ref string error, ref MySqlDataReader returned_reader, string room_no)
		{
			MySqlCommand cmd = new MySqlCommand();
			cmd.Connection = this.connection;
			try
			{
				cmd.CommandText = "SELECT * FROM guests WHERE room_no ='" + room_no + "'";
				MySqlDataReader reader = cmd.ExecuteReader();
				returned_reader = reader;
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool DeleteGuest(ref string error, string room_no)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;

				cmd.CommandText = "DELETE FROM guests WHERE room_no ='" + room_no + "'";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		
		public bool EditGuest(ref string error, string old_room_no, string new_room_no, string name, string phone_no, string mail, string ID_no, string credit_no, DateTime DOB, DateTime check_in_date, DateTime check_out_date)
		{
			Console.WriteLine(DOB);
			Console.WriteLine(DOB.ToString("d"));
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				if (old_room_no == new_room_no)
				{
					cmd.CommandText = "UPDATE guests SET name='" + name
									  + "', phone_no='" + phone_no
									  + "', mail='" + mail
									  + "', ID_no='" + ID_no
									  + "', credit_no='" + credit_no
									  + "', DOB='" + DOB.ToString("d")
									  + "', check_in_date='" + check_in_date.ToString("d")
									  + "', check_out_date='" + check_out_date.ToString("d")
									  + "' WHERE room_no ='" + old_room_no + "'";
					cmd.ExecuteNonQuery();
				}
				else
				{
					this.DeleteGuest(ref error, old_room_no);
					this.AddNewGuest(ref error, new_room_no, name, mail, ID_no, credit_no, phone_no, DOB, check_in_date, check_out_date);
				}
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool RetrieveTableData(ref string error, ref MySqlDataReader returned_reader, string table_name)
		{

			MySqlCommand cmd = new MySqlCommand();
			cmd.Connection = this.connection;
			try
			{
				cmd.CommandText = "SELECT * from " + table_name;
				MySqlDataReader reader = cmd.ExecuteReader();
				returned_reader = reader;
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}


		}

		public bool AddFeedbackQuestion(ref string error,string id,string value)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				cmd.CommandText = "INSERT INTO feedback_question(id,value)" +
								 " VALUES('" 
								 + id +"' , '"+
								 value + "')";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool DeleteFeedbackQuestion(ref string error, string id)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;

				cmd.CommandText = "DELETE FROM feedback_question WHERE id ='" + id + "'";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool EditFeedbackQuestion(ref string error, string old_id,string new_id, string new_value)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				if (old_id == new_id)
				{
					cmd.CommandText = "UPDATE feedback_question SET value='" + new_value
										+ "' WHERE id ='" + old_id + "'";

					cmd.ExecuteNonQuery();
				}
				else
				{
					this.DeleteFeedbackQuestion(ref error, old_id);
					this.AddFeedbackQuestion(ref error, new_id, new_value);
				}
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}


		public bool AddNewSound(ref string error, string id,string sound_name, string artist_names, string played_place)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				cmd.CommandText = "INSERT INTO sounds(id,sound_name, artist_names, played_place)" +
								 " VALUES('" + id + "', '"
								 + sound_name + "', '"
								 + artist_names + "', '"
								 + played_place + "')";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool DeleteSound(ref string error, string id)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;

				cmd.CommandText = "DELETE FROM sounds WHERE id ='" + id + "'";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool EditSound(ref string error,string old_id, string new_id, string sound_name, string artist_names, string played_place)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				if (old_id == new_id)
				{
					cmd.CommandText = "UPDATE sounds SET sound_name='" + sound_name
										+ "', artist_names='" + artist_names
										+ "', played_place='" + played_place
										+ "' WHERE id ='" + old_id + "'";
					cmd.ExecuteNonQuery();
				}
				else
				{
					this.DeleteSound(ref error, old_id);
					this.AddNewSound(ref error, new_id, sound_name, artist_names, played_place);
				}
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}


		public bool AddNewWifiPassword(ref string error, string room_no, string password)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				cmd.CommandText = "INSERT INTO wifipassword(room_no, password)" +
								 " VALUES('" + room_no + "', '"
								 + password + "')";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool GetWifiPassword(ref string error, ref MySqlDataReader returned_reader, string room_no)
		{
			MySqlCommand cmd = new MySqlCommand();
			cmd.Connection = this.connection;
			try
			{
				cmd.CommandText = "SELECT * FROM wifipassword WHERE room_no ='" + room_no + "'";
				MySqlDataReader reader = cmd.ExecuteReader();
				returned_reader = reader;
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool DeleteWifiPassword(ref string error, string room_no)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;

				cmd.CommandText = "DELETE FROM wifipassword WHERE room_no ='" + room_no + "'";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool EditWifiPassword(ref string error,string old_room_no, string new_room_no, string password)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				if (old_room_no == new_room_no)
				{
					cmd.CommandText = "UPDATE wifipassword SET password='" + password
									  + "' WHERE room_no ='" + old_room_no + "'";
					cmd.ExecuteNonQuery();
				}
				else
				{
					this.DeleteWifiPassword(ref error, old_room_no);
					this.AddNewWifiPassword(ref error, new_room_no, password);
				}
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}


		public bool AddNewPlaceRecommendation(ref string error,string id, string place_name, string place_address)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				cmd.CommandText = "INSERT INTO places_recommendations(id,place_name, place_address)" +
								 " VALUES('" + id + "', '"
								 + place_name + "', '"
								 + place_address + "')";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool DeletePlaceRecommendation(ref string error, string id)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;

				cmd.CommandText = "DELETE FROM places_recommendations WHERE id ='" + id + "'";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool EditPlaceRecommendation(ref string error, string old_id, string new_id, string place_name, string place_address)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				if (old_id == new_id)
				{
					cmd.CommandText = "UPDATE places_recommendations SET place_name='" + place_name
										+ "', place_address='" + place_address
										+ "' WHERE id ='" + old_id + "'";
					cmd.ExecuteNonQuery();
				}
				else
				{
					this.DeletePlaceRecommendation(ref error, old_id);
					this.AddNewPlaceRecommendation(ref error, new_id, place_name, place_address);
				}
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}

		public bool AddNewRestaurantRecommendation(ref string error,string id, string restaurant_name, string restaurant_address, string restaurant_type)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				cmd.CommandText = "INSERT INTO restaurants_recommendations(id,restaurant_name, restaurant_address, restaurant_type)" +
								 " VALUES('" + id +"' , '"+
								 restaurant_name + "', '"
								 + restaurant_address + "', '"
								 + restaurant_type + "')";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool DeleteRestaurantRecommendation(ref string error, string id)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;

				cmd.CommandText = "DELETE FROM restaurants_recommendations WHERE id ='" + id + "'";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool EditRestaurantRecommendation(ref string error, string old_id, string new_id, string restaurant_name, string restaurant_address, string restaurant_type)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				if (old_id == new_id)
				{
					cmd.CommandText = "UPDATE restaurants_recommendations SET restaurant_name='" + restaurant_name
										+ "', restaurant_address='" + restaurant_address
										+ "', restaurant_type='" + restaurant_type
										+ "' WHERE id ='" + old_id + "'";
					cmd.ExecuteNonQuery();
				}
				else
				{
					this.DeleteRestaurantRecommendation(ref error, old_id);
					this.AddNewRestaurantRecommendation(ref error, new_id, restaurant_name, restaurant_address, restaurant_type);
				}
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}

		public bool AddNewTransportationRecommendation(ref string error,string id, string destination_name, string method)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				cmd.CommandText = "INSERT INTO transportation_recommendations(id,destination_name, method)" +
								 " VALUES('" + id +"' , '" 
								 +destination_name + "', '"
								 + method + "')";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool DeleteTransportationRecommendation(ref string error, string id)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;

				cmd.CommandText = "DELETE FROM transportation_recommendations WHERE id ='" + id + "'";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool EditTransportationRecommendation(ref string error, string old_id,string new_id, string destination_name, string method)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				if (old_id == new_id)
				{
					cmd.CommandText = "UPDATE transportation_recommendations SET destination_name='" + destination_name
										+ "', method='" + method
										+ "' WHERE id ='" + old_id + "'";
					cmd.ExecuteNonQuery();
				}
				else
				{
					this.DeleteTransportationRecommendation(ref error, old_id);
					this.AddNewTransportationRecommendation(ref error, new_id, destination_name, method);
				}
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool AddNewAssistant(ref string error, int assistant_id,string room_no)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				cmd.CommandText = "INSERT INTO assistant_to_room_map(assistant_id, room_no)" +
								 " VALUES(" + assistant_id + ", '"
								 + room_no + "')";
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		
		public bool DeleteAssistant(ref string error, int assistant_id)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;

				cmd.CommandText = "DELETE FROM assistant_to_room_map WHERE assistant_id =" + assistant_id;
				cmd.ExecuteNonQuery();
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool EditAssistant(ref string error, int old_assistant_id, int new_assistant_id, string room_no)
		{
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = this.connection;
				if (old_assistant_id == new_assistant_id)
				{
					cmd.CommandText = "UPDATE assistant_to_room_map SET room_no='" + room_no
									  + "' WHERE assistant_id =" + old_assistant_id;
					cmd.ExecuteNonQuery();
				}
				else
				{
					this.DeleteAssistant(ref error, old_assistant_id);
					this.AddNewAssistant(ref error, new_assistant_id, room_no);
				}
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool GetRoom_no(ref string error, ref MySqlDataReader returned_reader, int assistant_id)
		{
			MySqlCommand cmd = new MySqlCommand();
			cmd.Connection = this.connection;
			try
			{
				cmd.CommandText = "SELECT * FROM assistant_to_room_map WHERE assistant_id =" + assistant_id;
				MySqlDataReader reader = cmd.ExecuteReader();
				returned_reader = reader;
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public bool GetAssistant_id(ref string error, ref MySqlDataReader returned_reader, string room_no)
		{
			MySqlCommand cmd = new MySqlCommand();
			cmd.Connection = this.connection;
			try
			{
				cmd.CommandText = "SELECT * FROM assistant_to_room_map WHERE room_no ='" + room_no +"'";
				MySqlDataReader reader = cmd.ExecuteReader();
				returned_reader = reader;
				error = "Success";
				return true;
			}
			catch (MySqlException ex)
			{
				error = "Error: {" + ex.ToString() + "}";
				Console.WriteLine(error);
				return false;
			}
		}
		public MySqlConnection get_connection()
		{
			return this.connection;
		}
		~DBConnect()
		{
			connection.Close();
			Console.WriteLine("Connection closed");
		}
	}
}
