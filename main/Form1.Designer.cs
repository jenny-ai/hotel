namespace Jenny
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.loginl = new System.Windows.Forms.CheckBox();
			this.registerl = new System.Windows.Forms.CheckBox();
			this.rememberCB = new System.Windows.Forms.CheckBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.login = new System.Windows.Forms.Button();
			this.register = new System.Windows.Forms.Panel();
			this.checkin = new System.Windows.Forms.Panel();
			this.tablespanel = new System.Windows.Forms.Panel();
			this.notify_panel = new System.Windows.Forms.Panel();
			this.dataGridView2 = new System.Windows.Forms.DataGridView();
			this.back_from_noti = new System.Windows.Forms.Button();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.delete_btn = new System.Windows.Forms.Button();
			this.Back = new System.Windows.Forms.Button();
			this.submitbtn = new System.Windows.Forms.Button();
			this.viewbtn = new System.Windows.Forms.Button();
			this.selecttable = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.creditTxt = new System.Windows.Forms.TextBox();
			this.label23 = new System.Windows.Forms.Label();
			this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
			this.label21 = new System.Windows.Forms.Label();
			this.notify_btn = new System.Windows.Forms.Button();
			this.edittablesbtn = new System.Windows.Forms.CheckBox();
			this.logout = new System.Windows.Forms.CheckBox();
			this.ncin = new System.Windows.Forms.CheckBox();
			this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.roomnumTxt = new System.Windows.Forms.TextBox();
			this.emailTxt = new System.Windows.Forms.TextBox();
			this.phoneTxt = new System.Windows.Forms.TextBox();
			this.idTxt = new System.Windows.Forms.TextBox();
			this.guestnameTxt = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.checkinbtn = new System.Windows.Forms.Button();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.Country_txtBox = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.City_txtBox = new System.Windows.Forms.TextBox();
			this.longitude_txtBox = new System.Windows.Forms.TextBox();
			this.label22 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.loginr = new System.Windows.Forms.CheckBox();
			this.registerr = new System.Windows.Forms.CheckBox();
			this.registerbtnr = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.HotelName_txtBox = new System.Windows.Forms.TextBox();
			this.UserName_txtBox = new System.Windows.Forms.TextBox();
			this.Password_txtBox = new System.Windows.Forms.TextBox();
			this.latitude_txtBox = new System.Windows.Forms.TextBox();
			this.DBPassword_txtBox = new System.Windows.Forms.TextBox();
			this.DBName_txtBox = new System.Windows.Forms.TextBox();
			this.DBPort_txtBox = new System.Windows.Forms.TextBox();
			this.DBUserName_txtBox = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.register.SuspendLayout();
			this.checkin.SuspendLayout();
			this.tablespanel.SuspendLayout();
			this.notify_panel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// loginl
			// 
			this.loginl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.loginl.Appearance = System.Windows.Forms.Appearance.Button;
			this.loginl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.loginl.Location = new System.Drawing.Point(494, 4);
			this.loginl.Name = "loginl";
			this.loginl.Size = new System.Drawing.Size(461, 101);
			this.loginl.TabIndex = 22;
			this.loginl.Text = "Login";
			this.loginl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.loginl.UseVisualStyleBackColor = true;
			// 
			// registerl
			// 
			this.registerl.Appearance = System.Windows.Forms.Appearance.Button;
			this.registerl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.registerl.Location = new System.Drawing.Point(1, 4);
			this.registerl.Name = "registerl";
			this.registerl.Size = new System.Drawing.Size(496, 101);
			this.registerl.TabIndex = 21;
			this.registerl.Text = "Register";
			this.registerl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.registerl.UseVisualStyleBackColor = true;
			this.registerl.CheckedChanged += new System.EventHandler(this.registerl_CheckedChanged);
			// 
			// rememberCB
			// 
			this.rememberCB.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.rememberCB.AutoSize = true;
			this.rememberCB.Location = new System.Drawing.Point(710, 563);
			this.rememberCB.Name = "rememberCB";
			this.rememberCB.Size = new System.Drawing.Size(117, 21);
			this.rememberCB.TabIndex = 19;
			this.rememberCB.Text = "remember me";
			this.rememberCB.UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.label3.Location = new System.Drawing.Point(108, 342);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(69, 17);
			this.label3.TabIndex = 18;
			this.label3.Text = "Password";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(108, 269);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(77, 17);
			this.label2.TabIndex = 17;
			this.label2.Text = "User name";
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(108, 198);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 17);
			this.label1.TabIndex = 16;
			this.label1.Text = "Hotel name";
			// 
			// textBox3
			// 
			this.textBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBox3.Location = new System.Drawing.Point(199, 335);
			this.textBox3.Name = "textBox3";
			this.textBox3.PasswordChar = '*';
			this.textBox3.Size = new System.Drawing.Size(665, 22);
			this.textBox3.TabIndex = 15;
			// 
			// textBox2
			// 
			this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBox2.Location = new System.Drawing.Point(199, 262);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(665, 22);
			this.textBox2.TabIndex = 14;
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.Location = new System.Drawing.Point(199, 192);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(665, 22);
			this.textBox1.TabIndex = 13;
			// 
			// login
			// 
			this.login.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.login.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.login.Location = new System.Drawing.Point(654, 449);
			this.login.Name = "login";
			this.login.Size = new System.Drawing.Size(214, 98);
			this.login.TabIndex = 23;
			this.login.Text = "Login";
			this.login.UseVisualStyleBackColor = true;
			this.login.Click += new System.EventHandler(this.login_Click);
			// 
			// register
			// 
			this.register.Controls.Add(this.checkin);
			this.register.Controls.Add(this.label15);
			this.register.Controls.Add(this.label14);
			this.register.Controls.Add(this.Country_txtBox);
			this.register.Controls.Add(this.label13);
			this.register.Controls.Add(this.City_txtBox);
			this.register.Controls.Add(this.longitude_txtBox);
			this.register.Controls.Add(this.label22);
			this.register.Controls.Add(this.label12);
			this.register.Controls.Add(this.loginr);
			this.register.Controls.Add(this.registerr);
			this.register.Controls.Add(this.registerbtnr);
			this.register.Controls.Add(this.checkBox1);
			this.register.Controls.Add(this.HotelName_txtBox);
			this.register.Controls.Add(this.UserName_txtBox);
			this.register.Controls.Add(this.Password_txtBox);
			this.register.Controls.Add(this.latitude_txtBox);
			this.register.Controls.Add(this.DBPassword_txtBox);
			this.register.Controls.Add(this.DBName_txtBox);
			this.register.Controls.Add(this.DBPort_txtBox);
			this.register.Controls.Add(this.DBUserName_txtBox);
			this.register.Controls.Add(this.label11);
			this.register.Controls.Add(this.label10);
			this.register.Controls.Add(this.label9);
			this.register.Controls.Add(this.label8);
			this.register.Controls.Add(this.label7);
			this.register.Controls.Add(this.label6);
			this.register.Controls.Add(this.label5);
			this.register.Controls.Add(this.label4);
			this.register.Dock = System.Windows.Forms.DockStyle.Fill;
			this.register.Location = new System.Drawing.Point(0, 0);
			this.register.Name = "register";
			this.register.Size = new System.Drawing.Size(955, 637);
			this.register.TabIndex = 24;
			this.register.Visible = false;
			// 
			// checkin
			// 
			this.checkin.Controls.Add(this.tablespanel);
			this.checkin.Controls.Add(this.creditTxt);
			this.checkin.Controls.Add(this.label23);
			this.checkin.Controls.Add(this.dateTimePicker3);
			this.checkin.Controls.Add(this.label21);
			this.checkin.Controls.Add(this.notify_btn);
			this.checkin.Controls.Add(this.edittablesbtn);
			this.checkin.Controls.Add(this.logout);
			this.checkin.Controls.Add(this.ncin);
			this.checkin.Controls.Add(this.dateTimePicker2);
			this.checkin.Controls.Add(this.dateTimePicker1);
			this.checkin.Controls.Add(this.roomnumTxt);
			this.checkin.Controls.Add(this.emailTxt);
			this.checkin.Controls.Add(this.phoneTxt);
			this.checkin.Controls.Add(this.idTxt);
			this.checkin.Controls.Add(this.guestnameTxt);
			this.checkin.Controls.Add(this.label16);
			this.checkin.Controls.Add(this.label17);
			this.checkin.Controls.Add(this.label18);
			this.checkin.Controls.Add(this.label19);
			this.checkin.Controls.Add(this.label20);
			this.checkin.Controls.Add(this.label24);
			this.checkin.Controls.Add(this.label25);
			this.checkin.Controls.Add(this.checkinbtn);
			this.checkin.Dock = System.Windows.Forms.DockStyle.Fill;
			this.checkin.Location = new System.Drawing.Point(0, 0);
			this.checkin.Name = "checkin";
			this.checkin.Size = new System.Drawing.Size(955, 637);
			this.checkin.TabIndex = 65;
			this.checkin.Visible = false;
			// 
			// tablespanel
			// 
			this.tablespanel.Controls.Add(this.notify_panel);
			this.tablespanel.Controls.Add(this.dataGridView1);
			this.tablespanel.Controls.Add(this.delete_btn);
			this.tablespanel.Controls.Add(this.Back);
			this.tablespanel.Controls.Add(this.submitbtn);
			this.tablespanel.Controls.Add(this.viewbtn);
			this.tablespanel.Controls.Add(this.selecttable);
			this.tablespanel.Controls.Add(this.comboBox1);
			this.tablespanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tablespanel.Location = new System.Drawing.Point(0, 0);
			this.tablespanel.Name = "tablespanel";
			this.tablespanel.Size = new System.Drawing.Size(955, 637);
			this.tablespanel.TabIndex = 56;
			this.tablespanel.Visible = false;
			// 
			// notify_panel
			// 
			this.notify_panel.Controls.Add(this.dataGridView2);
			this.notify_panel.Controls.Add(this.back_from_noti);
			this.notify_panel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.notify_panel.Location = new System.Drawing.Point(0, 0);
			this.notify_panel.Name = "notify_panel";
			this.notify_panel.Size = new System.Drawing.Size(955, 637);
			this.notify_panel.TabIndex = 58;
			this.notify_panel.Visible = false;
			// 
			// dataGridView2
			// 
			this.dataGridView2.AllowUserToAddRows = false;
			this.dataGridView2.AllowUserToDeleteRows = false;
			this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView2.Location = new System.Drawing.Point(3, 10);
			this.dataGridView2.Name = "dataGridView2";
			this.dataGridView2.ReadOnly = true;
			this.dataGridView2.RowTemplate.Height = 29;
			this.dataGridView2.Size = new System.Drawing.Size(952, 497);
			this.dataGridView2.TabIndex = 1;
			// 
			// back_from_noti
			// 
			this.back_from_noti.Location = new System.Drawing.Point(703, 513);
			this.back_from_noti.Name = "back_from_noti";
			this.back_from_noti.Size = new System.Drawing.Size(230, 57);
			this.back_from_noti.TabIndex = 0;
			this.back_from_noti.Text = "Back";
			this.back_from_noti.UseVisualStyleBackColor = true;
			this.back_from_noti.Click += new System.EventHandler(this.back_from_noti_Click);
			// 
			// dataGridView1
			// 
			this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Location = new System.Drawing.Point(0, 62);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowTemplate.Height = 29;
			this.dataGridView1.Size = new System.Drawing.Size(955, 522);
			this.dataGridView1.TabIndex = 56;
			this.dataGridView1.Visible = false;
			// 
			// delete_btn
			// 
			this.delete_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.delete_btn.Location = new System.Drawing.Point(743, 589);
			this.delete_btn.Name = "delete_btn";
			this.delete_btn.Size = new System.Drawing.Size(190, 39);
			this.delete_btn.TabIndex = 55;
			this.delete_btn.Text = "Delete Row";
			this.delete_btn.UseVisualStyleBackColor = true;
			this.delete_btn.Click += new System.EventHandler(this.delete_btn_Click);
			// 
			// Back
			// 
			this.Back.Location = new System.Drawing.Point(743, 4);
			this.Back.Name = "Back";
			this.Back.Size = new System.Drawing.Size(190, 52);
			this.Back.TabIndex = 54;
			this.Back.Text = "Back";
			this.Back.UseVisualStyleBackColor = true;
			this.Back.Click += new System.EventHandler(this.Back_Click);
			// 
			// submitbtn
			// 
			this.submitbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.submitbtn.Location = new System.Drawing.Point(11, 589);
			this.submitbtn.Name = "submitbtn";
			this.submitbtn.Size = new System.Drawing.Size(203, 37);
			this.submitbtn.TabIndex = 49;
			this.submitbtn.Text = "Submit";
			this.submitbtn.UseVisualStyleBackColor = true;
			this.submitbtn.Click += new System.EventHandler(this.submitbtn_Click_1);
			// 
			// viewbtn
			// 
			this.viewbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.viewbtn.Location = new System.Drawing.Point(360, 3);
			this.viewbtn.Name = "viewbtn";
			this.viewbtn.Size = new System.Drawing.Size(180, 53);
			this.viewbtn.TabIndex = 47;
			this.viewbtn.Text = "View";
			this.viewbtn.UseVisualStyleBackColor = true;
			this.viewbtn.Click += new System.EventHandler(this.viewbtn_Click_1);
			// 
			// selecttable
			// 
			this.selecttable.AutoSize = true;
			this.selecttable.Location = new System.Drawing.Point(18, 22);
			this.selecttable.Name = "selecttable";
			this.selecttable.Size = new System.Drawing.Size(87, 17);
			this.selecttable.TabIndex = 46;
			this.selecttable.Text = "Select Table";
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
            "Assistant Room Map",
            "Feedback Question",
            "Guests",
            "Places Recommendation",
            "Restaurants Recommendation",
            "Sounds",
            "Transportation Recommendation",
            "Wifi Password"});
			this.comboBox1.Location = new System.Drawing.Point(119, 22);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(175, 24);
			this.comboBox1.Sorted = true;
			this.comboBox1.TabIndex = 45;
			// 
			// creditTxt
			// 
			this.creditTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.creditTxt.Location = new System.Drawing.Point(306, 213);
			this.creditTxt.Name = "creditTxt";
			this.creditTxt.Size = new System.Drawing.Size(518, 22);
			this.creditTxt.TabIndex = 55;
			// 
			// label23
			// 
			this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(162, 221);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(131, 17);
			this.label23.TabIndex = 54;
			this.label23.Text = "credit Card Number";
			// 
			// dateTimePicker3
			// 
			this.dateTimePicker3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dateTimePicker3.Location = new System.Drawing.Point(306, 453);
			this.dateTimePicker3.Name = "dateTimePicker3";
			this.dateTimePicker3.Size = new System.Drawing.Size(518, 22);
			this.dateTimePicker3.TabIndex = 52;
			// 
			// label21
			// 
			this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(162, 456);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(106, 17);
			this.label21.TabIndex = 50;
			this.label21.Text = "Check-out Date";
			// 
			// notify_btn
			// 
			this.notify_btn.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.notify_btn.Location = new System.Drawing.Point(493, 0);
			this.notify_btn.Name = "notify_btn";
			this.notify_btn.Size = new System.Drawing.Size(226, 79);
			this.notify_btn.TabIndex = 44;
			this.notify_btn.Text = "Notifications";
			this.notify_btn.UseVisualStyleBackColor = true;
			this.notify_btn.Click += new System.EventHandler(this.notify_btn_Click);
			// 
			// edittablesbtn
			// 
			this.edittablesbtn.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.edittablesbtn.Appearance = System.Windows.Forms.Appearance.Button;
			this.edittablesbtn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.edittablesbtn.Location = new System.Drawing.Point(262, 0);
			this.edittablesbtn.Name = "edittablesbtn";
			this.edittablesbtn.Size = new System.Drawing.Size(233, 79);
			this.edittablesbtn.TabIndex = 42;
			this.edittablesbtn.Text = "Edit Tables";
			this.edittablesbtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.edittablesbtn.UseVisualStyleBackColor = true;
			this.edittablesbtn.CheckedChanged += new System.EventHandler(this.edittablesbtn_CheckedChanged);
			// 
			// logout
			// 
			this.logout.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.logout.Appearance = System.Windows.Forms.Appearance.Button;
			this.logout.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.logout.Location = new System.Drawing.Point(714, 0);
			this.logout.Name = "logout";
			this.logout.Size = new System.Drawing.Size(238, 79);
			this.logout.TabIndex = 40;
			this.logout.Text = "Logout";
			this.logout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.logout.UseVisualStyleBackColor = true;
			this.logout.CheckedChanged += new System.EventHandler(this.logout_CheckedChanged_1);
			// 
			// ncin
			// 
			this.ncin.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.ncin.Appearance = System.Windows.Forms.Appearance.Button;
			this.ncin.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ncin.Location = new System.Drawing.Point(3, 0);
			this.ncin.Name = "ncin";
			this.ncin.Size = new System.Drawing.Size(264, 79);
			this.ncin.TabIndex = 38;
			this.ncin.Text = "New Check-in";
			this.ncin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.ncin.UseVisualStyleBackColor = true;
			// 
			// dateTimePicker2
			// 
			this.dateTimePicker2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dateTimePicker2.Location = new System.Drawing.Point(306, 419);
			this.dateTimePicker2.Name = "dateTimePicker2";
			this.dateTimePicker2.Size = new System.Drawing.Size(518, 22);
			this.dateTimePicker2.TabIndex = 37;
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dateTimePicker1.Location = new System.Drawing.Point(306, 373);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(518, 22);
			this.dateTimePicker1.TabIndex = 36;
			// 
			// roomnumTxt
			// 
			this.roomnumTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.roomnumTxt.Location = new System.Drawing.Point(306, 336);
			this.roomnumTxt.Name = "roomnumTxt";
			this.roomnumTxt.Size = new System.Drawing.Size(518, 22);
			this.roomnumTxt.TabIndex = 35;
			// 
			// emailTxt
			// 
			this.emailTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.emailTxt.Location = new System.Drawing.Point(306, 294);
			this.emailTxt.Name = "emailTxt";
			this.emailTxt.Size = new System.Drawing.Size(518, 22);
			this.emailTxt.TabIndex = 34;
			// 
			// phoneTxt
			// 
			this.phoneTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.phoneTxt.Location = new System.Drawing.Point(306, 255);
			this.phoneTxt.Name = "phoneTxt";
			this.phoneTxt.Size = new System.Drawing.Size(518, 22);
			this.phoneTxt.TabIndex = 33;
			// 
			// idTxt
			// 
			this.idTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.idTxt.Location = new System.Drawing.Point(306, 175);
			this.idTxt.Name = "idTxt";
			this.idTxt.Size = new System.Drawing.Size(518, 22);
			this.idTxt.TabIndex = 32;
			// 
			// guestnameTxt
			// 
			this.guestnameTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.guestnameTxt.Location = new System.Drawing.Point(306, 136);
			this.guestnameTxt.Name = "guestnameTxt";
			this.guestnameTxt.Size = new System.Drawing.Size(518, 22);
			this.guestnameTxt.TabIndex = 31;
			// 
			// label16
			// 
			this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(162, 419);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(97, 17);
			this.label16.TabIndex = 30;
			this.label16.Text = "Check-in Date";
			// 
			// label17
			// 
			this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(162, 381);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(90, 17);
			this.label17.TabIndex = 29;
			this.label17.Text = "Date Of Birth";
			// 
			// label18
			// 
			this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(162, 344);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(99, 17);
			this.label18.TabIndex = 28;
			this.label18.Text = "Room Number";
			// 
			// label19
			// 
			this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(162, 303);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(47, 17);
			this.label19.TabIndex = 27;
			this.label19.Text = "E-mail";
			// 
			// label20
			// 
			this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(162, 262);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(103, 17);
			this.label20.TabIndex = 26;
			this.label20.Text = "Phone Number";
			// 
			// label24
			// 
			this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(162, 182);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(75, 17);
			this.label24.TabIndex = 25;
			this.label24.Text = "ID Number";
			// 
			// label25
			// 
			this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(162, 136);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(87, 17);
			this.label25.TabIndex = 24;
			this.label25.Text = "Guest Name";
			// 
			// checkinbtn
			// 
			this.checkinbtn.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.checkinbtn.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.checkinbtn.Location = new System.Drawing.Point(360, 513);
			this.checkinbtn.Name = "checkinbtn";
			this.checkinbtn.Size = new System.Drawing.Size(318, 91);
			this.checkinbtn.TabIndex = 23;
			this.checkinbtn.Text = "Check-in";
			this.checkinbtn.UseVisualStyleBackColor = true;
			this.checkinbtn.Click += new System.EventHandler(this.checkinbtn_Click);
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(511, 136);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(143, 17);
			this.label15.TabIndex = 64;
			this.label15.Text = "Database Information";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(11, 136);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(115, 17);
			this.label14.TabIndex = 63;
			this.label14.Text = "Hotel Information";
			// 
			// Country_txtBox
			// 
			this.Country_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Country_txtBox.Location = new System.Drawing.Point(165, 243);
			this.Country_txtBox.Name = "Country_txtBox";
			this.Country_txtBox.Size = new System.Drawing.Size(292, 22);
			this.Country_txtBox.TabIndex = 62;
			// 
			// label13
			// 
			this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(37, 247);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(57, 17);
			this.label13.TabIndex = 61;
			this.label13.Text = "Country";
			// 
			// City_txtBox
			// 
			this.City_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.City_txtBox.Location = new System.Drawing.Point(165, 283);
			this.City_txtBox.Name = "City_txtBox";
			this.City_txtBox.Size = new System.Drawing.Size(292, 22);
			this.City_txtBox.TabIndex = 60;
			// 
			// longitude_txtBox
			// 
			this.longitude_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.longitude_txtBox.Location = new System.Drawing.Point(165, 376);
			this.longitude_txtBox.Name = "longitude_txtBox";
			this.longitude_txtBox.Size = new System.Drawing.Size(292, 22);
			this.longitude_txtBox.TabIndex = 59;
			// 
			// label22
			// 
			this.label22.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(37, 376);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(71, 17);
			this.label22.TabIndex = 58;
			this.label22.Text = "Longitude";
			// 
			// label12
			// 
			this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(37, 285);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(31, 17);
			this.label12.TabIndex = 57;
			this.label12.Text = "City";
			// 
			// loginr
			// 
			this.loginr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.loginr.Appearance = System.Windows.Forms.Appearance.Button;
			this.loginr.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.loginr.Location = new System.Drawing.Point(493, 0);
			this.loginr.Name = "loginr";
			this.loginr.Size = new System.Drawing.Size(461, 121);
			this.loginr.TabIndex = 45;
			this.loginr.Text = "Login";
			this.loginr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.loginr.UseVisualStyleBackColor = true;
			this.loginr.CheckedChanged += new System.EventHandler(this.loginr_CheckedChanged_1);
			// 
			// registerr
			// 
			this.registerr.Appearance = System.Windows.Forms.Appearance.Button;
			this.registerr.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.registerr.Location = new System.Drawing.Point(0, 0);
			this.registerr.Name = "registerr";
			this.registerr.Size = new System.Drawing.Size(495, 121);
			this.registerr.TabIndex = 44;
			this.registerr.Text = "Register";
			this.registerr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.registerr.UseVisualStyleBackColor = true;
			// 
			// registerbtnr
			// 
			this.registerbtnr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.registerbtnr.Enabled = false;
			this.registerbtnr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.registerbtnr.Location = new System.Drawing.Point(494, 535);
			this.registerbtnr.Name = "registerbtnr";
			this.registerbtnr.Size = new System.Drawing.Size(268, 91);
			this.registerbtnr.TabIndex = 43;
			this.registerbtnr.Text = "Register";
			this.registerbtnr.UseVisualStyleBackColor = true;
			this.registerbtnr.Click += new System.EventHandler(this.registerbtnr_Click_1);
			// 
			// checkBox1
			// 
			this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.checkBox1.AutoSize = true;
			this.checkBox1.Location = new System.Drawing.Point(220, 588);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(249, 21);
			this.checkBox1.TabIndex = 42;
			this.checkBox1.Text = "I have read and agree to the terms";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
			// 
			// HotelName_txtBox
			// 
			this.HotelName_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.HotelName_txtBox.Location = new System.Drawing.Point(165, 203);
			this.HotelName_txtBox.Name = "HotelName_txtBox";
			this.HotelName_txtBox.Size = new System.Drawing.Size(292, 22);
			this.HotelName_txtBox.TabIndex = 41;
			// 
			// UserName_txtBox
			// 
			this.UserName_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.UserName_txtBox.Location = new System.Drawing.Point(165, 419);
			this.UserName_txtBox.Name = "UserName_txtBox";
			this.UserName_txtBox.Size = new System.Drawing.Size(292, 22);
			this.UserName_txtBox.TabIndex = 40;
			// 
			// Password_txtBox
			// 
			this.Password_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Password_txtBox.Location = new System.Drawing.Point(165, 465);
			this.Password_txtBox.Name = "Password_txtBox";
			this.Password_txtBox.PasswordChar = '*';
			this.Password_txtBox.Size = new System.Drawing.Size(292, 22);
			this.Password_txtBox.TabIndex = 39;
			// 
			// latitude_txtBox
			// 
			this.latitude_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.latitude_txtBox.Location = new System.Drawing.Point(165, 329);
			this.latitude_txtBox.Name = "latitude_txtBox";
			this.latitude_txtBox.Size = new System.Drawing.Size(292, 22);
			this.latitude_txtBox.TabIndex = 38;
			// 
			// DBPassword_txtBox
			// 
			this.DBPassword_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.DBPassword_txtBox.Location = new System.Drawing.Point(631, 235);
			this.DBPassword_txtBox.Name = "DBPassword_txtBox";
			this.DBPassword_txtBox.PasswordChar = '*';
			this.DBPassword_txtBox.Size = new System.Drawing.Size(313, 22);
			this.DBPassword_txtBox.TabIndex = 37;
			// 
			// DBName_txtBox
			// 
			this.DBName_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.DBName_txtBox.Location = new System.Drawing.Point(631, 269);
			this.DBName_txtBox.Name = "DBName_txtBox";
			this.DBName_txtBox.Size = new System.Drawing.Size(313, 22);
			this.DBName_txtBox.TabIndex = 36;
			// 
			// DBPort_txtBox
			// 
			this.DBPort_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.DBPort_txtBox.Location = new System.Drawing.Point(631, 306);
			this.DBPort_txtBox.Name = "DBPort_txtBox";
			this.DBPort_txtBox.Size = new System.Drawing.Size(313, 22);
			this.DBPort_txtBox.TabIndex = 35;
			// 
			// DBUserName_txtBox
			// 
			this.DBUserName_txtBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.DBUserName_txtBox.Location = new System.Drawing.Point(631, 200);
			this.DBUserName_txtBox.Name = "DBUserName_txtBox";
			this.DBUserName_txtBox.Size = new System.Drawing.Size(313, 22);
			this.DBUserName_txtBox.TabIndex = 34;
			// 
			// label11
			// 
			this.label11.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(523, 304);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(41, 17);
			this.label11.TabIndex = 33;
			this.label11.Text = "port#";
			// 
			// label10
			// 
			this.label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(523, 272);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(108, 17);
			this.label10.TabIndex = 32;
			this.label10.Text = "Database name";
			// 
			// label9
			// 
			this.label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(523, 235);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(69, 17);
			this.label9.TabIndex = 31;
			this.label9.Text = "Password";
			// 
			// label8
			// 
			this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(523, 203);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(77, 17);
			this.label8.TabIndex = 30;
			this.label8.Text = "User name";
			// 
			// label7
			// 
			this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(37, 329);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(59, 17);
			this.label7.TabIndex = 29;
			this.label7.Text = "Latitude";
			// 
			// label6
			// 
			this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(37, 461);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(69, 17);
			this.label6.TabIndex = 28;
			this.label6.Text = "Password";
			// 
			// label5
			// 
			this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(37, 419);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(77, 17);
			this.label5.TabIndex = 27;
			this.label5.Text = "User name";
			// 
			// label4
			// 
			this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(37, 201);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(80, 17);
			this.label4.TabIndex = 26;
			this.label4.Text = "Hotel name";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.BackgroundImage = global::Jenny.Properties.Resources.background1;
			this.ClientSize = new System.Drawing.Size(955, 637);
			this.Controls.Add(this.register);
			this.Controls.Add(this.login);
			this.Controls.Add(this.loginl);
			this.Controls.Add(this.registerl);
			this.Controls.Add(this.rememberCB);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form1";
			this.Text = " log In";
			this.register.ResumeLayout(false);
			this.register.PerformLayout();
			this.checkin.ResumeLayout(false);
			this.checkin.PerformLayout();
			this.tablespanel.ResumeLayout(false);
			this.tablespanel.PerformLayout();
			this.notify_panel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox loginl;
        private System.Windows.Forms.CheckBox registerl;
        private System.Windows.Forms.CheckBox rememberCB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button login;
        private System.Windows.Forms.Panel register;
        private System.Windows.Forms.CheckBox loginr;
        private System.Windows.Forms.CheckBox registerr;
        private System.Windows.Forms.Button registerbtnr;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox HotelName_txtBox;
        private System.Windows.Forms.TextBox UserName_txtBox;
        private System.Windows.Forms.TextBox Password_txtBox;
        private System.Windows.Forms.TextBox latitude_txtBox;
        private System.Windows.Forms.TextBox DBPassword_txtBox;
        private System.Windows.Forms.TextBox DBName_txtBox;
        private System.Windows.Forms.TextBox DBPort_txtBox;
        private System.Windows.Forms.TextBox DBUserName_txtBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Country_txtBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox City_txtBox;
        private System.Windows.Forms.TextBox longitude_txtBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel checkin;
        private System.Windows.Forms.Panel tablespanel;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button delete_btn;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Button submitbtn;
        private System.Windows.Forms.Button viewbtn;
        private System.Windows.Forms.Label selecttable;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox creditTxt;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button notify_btn;
        private System.Windows.Forms.CheckBox edittablesbtn;
        private System.Windows.Forms.CheckBox logout;
        private System.Windows.Forms.CheckBox ncin;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox roomnumTxt;
        private System.Windows.Forms.TextBox emailTxt;
        private System.Windows.Forms.TextBox phoneTxt;
        private System.Windows.Forms.TextBox idTxt;
        private System.Windows.Forms.TextBox guestnameTxt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button checkinbtn;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel notify_panel;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button back_from_noti;
    }
}

