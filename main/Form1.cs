using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Tulpep.NotificationWindow;
using MySql.Data.MySqlClient;
using System.IO;
using Jenny.Properties;


namespace Jenny
{
	
	public partial class Form1 : Form
    {
        Dictionary<string, string> TABLESNAME = new Dictionary<string, string>();
        List<string> IDs = new List<string>();
		Dictionary<string, string> HotelInfo;		DataTable dt = new DataTable();
		Client c;

		public Form1()
        {
            InitializeComponent();
            loginl.Select();
            
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            TABLESNAME.Add("Guests", "guests");
            TABLESNAME.Add("Places Recommendation", "places_recommendations");
            TABLESNAME.Add("Restaurants Recommendation", "restaurants_recommendations");
            TABLESNAME.Add("Sounds", "sounds");
            TABLESNAME.Add("Transportation Recommendation", "transportation_recommendations");
            TABLESNAME.Add("Wifi Password", "wifipassword");
            TABLESNAME.Add("Feedback Question", "feedback_question");
            TABLESNAME.Add("Transportation", "transportation_recommendations");
            TABLESNAME.Add("Assistant Room Map", "assistant_to_room_map");
            textBox1.Text = Settings.Default.hotelname ;
            textBox2.Text = Settings.Default.username;
            textBox3.Text = Settings.Default.pass;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.rememberCB.BackColor = System.Drawing.Color.Transparent;

        }
        
        private void registerl_CheckedChanged(object sender, EventArgs e)
        {
            checkin.Visible = false;
            register.Visible = true;
            registerr.Select();
            checkBox1.Checked = false;
            this.Text = ("Register");

        }

        

        private void loginr_CheckedChanged(object sender, EventArgs e)
        {
            register.Visible = false;
            loginl.Select();
            this.Text = ("Login");
        }
        

       
        private void login_Click(object sender, EventArgs e)
        {
			//register.Visible = true;
            //checkin.Visible = true;
            //this.Text = ("Check-in");
            if(rememberCB.Checked)
            {
                Jenny.Properties.Settings.Default.hotelname = textBox1.Text;
                Jenny.Properties.Settings.Default.username = textBox2.Text;
                Jenny.Properties.Settings.Default.pass = textBox3.Text;
                Jenny.Properties.Settings.Default.Save();
            }
            else 
            {
                Jenny.Properties.Settings.Default.hotelname = null;
                Jenny.Properties.Settings.Default.username = null;
                Jenny.Properties.Settings.Default.pass = null;
                Jenny.Properties.Settings.Default.Save();
            }
			StreamReader r;
			try
			{
				r = new System.IO.StreamReader("hotelInfo.json");
				var json = r.ReadToEnd();
				HotelInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
				string[] keys = new string[11] { "HotelName", "Country", "City", "Latitude", "Longitude", "UserName", "Password", "DBUser", "DBPassword", "DBName", "DBPort#" };
				
				foreach (string key in keys)
				{
					if (!HotelInfo.ContainsKey(key))
					{
						MessageBox.Show("No Hotels registered");
					}
				}
				if (HotelInfo["HotelName"] == textBox1.Text && HotelInfo["UserName"] == textBox2.Text && HotelInfo["Password"] == textBox3.Text)
				{
					register.Visible = true;
					checkin.Visible = true;
					this.Text = ("Check-in");
					Console.WriteLine("start mqtt client");
					//Client c = new Client("192.168.191.119");
					Console.WriteLine("start done");
				}
				else if (HotelInfo["HotelName"] != textBox1.Text || HotelInfo["UserName"] != textBox2.Text || HotelInfo["Password"] != textBox3.Text)
				{
					MessageBox.Show("Invalid account data");

				}

			}
			catch
			{
				MessageBox.Show("No Hotels registered");
				register.Visible = true;
			}
			textBox1.Text = ""; textBox2.Text = ""; textBox3.Text = "";
			

		}



		private void loginr_CheckedChanged_1(object sender, EventArgs e)
        {
            checkin.Visible = true;
            register.Visible = false;
            this.Text = ("Login");
        }

        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
                registerbtnr.Enabled = true;
            else
                registerbtnr.Enabled = false;
        }

        private void registerbtnr_Click_1(object sender, EventArgs e)
        {
			/*register.Visible = true;
            checkin.Visible = true;
            this.Text = ("Check-in");*/
			register.Visible = true;
			login.Visible = true;
			Dictionary<string, string> hotelInfo = new Dictionary<string, string>();

			//Console.WriteLine(json);
			if (HotelName_txtBox.Text != "" &&
				Country_txtBox.Text != "" &&
				City_txtBox.Text != "" &&
				latitude_txtBox.Text != "" &&
				longitude_txtBox.Text != "" &&
				UserName_txtBox.Text != "" &&
				Password_txtBox.Text != "" &&
				DBUserName_txtBox.Text != "" &&
				DBPassword_txtBox.Text != "" &&
				DBName_txtBox.Text != "" &&
				DBPort_txtBox.Text != "")
			{
				hotelInfo.Add("HotelName", HotelName_txtBox.Text);
				hotelInfo.Add("Country", Country_txtBox.Text);
				hotelInfo.Add("City", City_txtBox.Text);
				hotelInfo.Add("Latitude", latitude_txtBox.Text);
				hotelInfo.Add("Longitude", longitude_txtBox.Text);
				hotelInfo.Add("UserName", UserName_txtBox.Text);
				hotelInfo.Add("Password", Password_txtBox.Text);
				hotelInfo.Add("DBUser", DBUserName_txtBox.Text);
				hotelInfo.Add("DBPassword", DBPassword_txtBox.Text);
				hotelInfo.Add("DBName", DBName_txtBox.Text);
				hotelInfo.Add("DBPort#", DBPort_txtBox.Text);
				var entries = hotelInfo.Select(d =>
				string.Format("\"{0}\": \"{1}\"", d.Key, string.Join(",", d.Value)));
				string HotelInfojson = "{" + string.Join(",", entries) + "}";

				StreamWriter sw = new StreamWriter("HotelInfo.json");
				sw.Write(HotelInfojson);
				sw.Close();
				MessageBox.Show("Registered successfully!");
				login.Visible = true;
				register.Visible = false;
				loginl.Select();
				this.Text = ("login");

				HotelName_txtBox.Text = ""; Country_txtBox.Text = ""; City_txtBox.Text = ""; latitude_txtBox.Text = ""; longitude_txtBox.Text = ""; UserName_txtBox.Text = ""; Password_txtBox.Text = ""; DBUserName_txtBox.Text = ""; DBPassword_txtBox.Text = ""; DBName_txtBox.Text = ""; DBPort_txtBox.Text = "";
			}
			else
			{
				MessageBox.Show("You need to fill all fields", "error");
			}


		}







		private void tableinsertbtn_Click(object sender, EventArgs e)
        {
            tablespanel.Visible = false;
            this.Text = ("check-in");
        }

        private void notifications_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PopupNotifier popup = new PopupNotifier();
           // popup.Image = Properties.Resources.info;
            popup.TitleText = "Emergency";
            popup.ContentText = "help room number whatever!";
            popup.Popup();
        }

       
        private void show_table(string table_name)
        {
            dataGridView1.DataSource = dt;
            DBConnect a = new DBConnect( HotelInfo["DBUser"], HotelInfo["DBName"], HotelInfo["DBPassword"]);
            MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter("select * from "+ table_name, a.get_connection());
            DataSet DS = new DataSet();
            mySqlDataAdapter.Fill(DS);
            dataGridView1.DataSource = DS.Tables[0];

        }

		private void backbtn_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = false;
            tablespanel.Visible = false; 
        }

    


       

 

        private void viewbtn_Click_1(object sender, EventArgs e)
        {
            IDs.Clear();
            if (comboBox1.SelectedIndex != -1)
            {
                show_table(TABLESNAME[comboBox1.Text]);
            }
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (dataGridView1[0, i].Value.ToString() != "")
                    IDs.Add(dataGridView1[0, i].Value.ToString());
            }
        }

        private void Back_Click(object sender, EventArgs e)
        {
			this.Text = ("check-in");
			dataGridView1.Visible = false;
            tablespanel.Visible = false;
        }

        private void logout_CheckedChanged_1(object sender, EventArgs e)
        {
            checkin.Visible = false;
            register.Visible = false;
            registerl.Checked = false;
            this.Text = ("Login");
            logout.Checked = false;
            textBox1.Text = Settings.Default.hotelname;
            textBox2.Text = Settings.Default.username;
            textBox3.Text = Settings.Default.pass;
        }

        private void checkinbtn_Click(object sender, EventArgs e)
        {
            string error = "";
            DBConnect a = new DBConnect( HotelInfo["DBUser"], HotelInfo["DBName"], HotelInfo["DBPassword"]);
            bool output = a.AddNewGuest(ref error, roomnumTxt.Text, guestnameTxt.Text, emailTxt.Text, phoneTxt.Text, idTxt.Text, creditTxt.Text, this.dateTimePicker1.Value, this.dateTimePicker2.Value, this.dateTimePicker3.Value);
            Console.WriteLine(error);
			if (output == true)
			{
				MessageBox.Show("Guest Added Successfully!");
				c.send_guest_data(roomnumTxt.Text);
			}
			else
				MessageBox.Show(error);
            creditTxt.Clear();
            guestnameTxt.Clear();
            phoneTxt.Clear();
            idTxt.Clear();
            roomnumTxt.Clear();
            emailTxt.Clear();
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
            dateTimePicker3.Value = DateTime.Now;
        }

        private void edittablesbtn_CheckedChanged(object sender, EventArgs e)
        {
            tablespanel.Visible = true;
            dataGridView1.Visible = true;
            this.Text = ("Table Editting");
        }

       

        private void submitbtn_Click_1(object sender, EventArgs e)
        {
            DBConnect a = new DBConnect(HotelInfo["DBUser"], HotelInfo["DBName"], HotelInfo["DBPassword"]);
            string error = " ";
            if (TABLESNAME[comboBox1.Text] == "guests")
            {
                if (dataGridView1.Rows.Count - 1 > IDs.Count)
                {
                    int ids = IDs.Count;
                    int records_number = (dataGridView1.Rows.Count - 1) - ids;

                    for (int y = 0; y < records_number; y++)
                    {
                        IDs.Add(dataGridView1[0, ids + y].Value.ToString());
                        bool result = a.AddNewGuest(ref error, dataGridView1[0, ids + y].Value.ToString(), dataGridView1[1, ids+y].Value.ToString(), dataGridView1[2, ids+y].Value.ToString(), dataGridView1[3, ids+y].Value.ToString(), dataGridView1[4, ids+y].Value.ToString(), dataGridView1[5, ids+y].Value.ToString(), Convert.ToDateTime(dataGridView1[6, ids+y].Value), Convert.ToDateTime(dataGridView1[7, ids+y].Value), Convert.ToDateTime(dataGridView1[8, ids+y].Value));
						if (result)
						{
							c.send_guest_data(dataGridView1[0, ids + y].Value.ToString());
						}
                    }
                }
                for (int j = 0; j < dataGridView1.Rows.Count - 1; j++)
                {
					bool result = a.EditGuest(ref error, IDs[j], dataGridView1[0, j].Value.ToString(), dataGridView1[1, j].Value.ToString(), dataGridView1[2, j].Value.ToString(), dataGridView1[3, j].Value.ToString(), dataGridView1[4, j].Value.ToString(), dataGridView1[5, j].Value.ToString(), Convert.ToDateTime(dataGridView1[6, j].Value), Convert.ToDateTime(dataGridView1[7, j].Value), Convert.ToDateTime(dataGridView1[8, j].Value));
					if (result)
					{
						c.send_guest_data(IDs[j]);
					}
				}
            }
            if (TABLESNAME[comboBox1.Text] == "feedback_question")
            {
                if (dataGridView1.Rows.Count - 1 > IDs.Count)
                {
                    int ids = IDs.Count;
                    int records_number = (dataGridView1.Rows.Count - 1) - ids;

                    for (int y = 0; y < records_number; y++)
                    {
                        IDs.Add(dataGridView1[0, ids + y].Value.ToString());
                        a.AddFeedbackQuestion(ref error, dataGridView1[0, ids + y].Value.ToString(), dataGridView1[1, ids + y].Value.ToString());
                    }
                 }

                for (int j = 0; j < dataGridView1.Rows.Count - 1; j++)
                {

                    a.EditFeedbackQuestion(ref error, IDs[j], dataGridView1[0, j].Value.ToString(), dataGridView1[1, j].Value.ToString());

                }
            }
            if (TABLESNAME[comboBox1.Text] == "places_recommendations")
            {
                if (dataGridView1.Rows.Count - 1 > IDs.Count)
                {
                    int ids = IDs.Count;
                    int records_number = (dataGridView1.Rows.Count - 1) - ids;

                    for (int y = 0; y < records_number; y++)
                    {
                        IDs.Add(dataGridView1[0, ids + y].Value.ToString());
                        a.AddNewPlaceRecommendation(ref error, dataGridView1[0, ids+y].Value.ToString(), dataGridView1[1, ids+y].Value.ToString(), dataGridView1[2, ids + y].Value.ToString());
                    }
                }
                for (int j = 0; j < dataGridView1.Rows.Count - 1; j++)
                {

                    a.EditPlaceRecommendation(ref error, IDs[j], dataGridView1[0, j].Value.ToString(), dataGridView1[1, j].Value.ToString(), dataGridView1[2, j].Value.ToString());

                }
            }
            if (TABLESNAME[comboBox1.Text] == "restaurants_recommendations")
            {
                if (dataGridView1.Rows.Count - 1 > IDs.Count)
                {
                    int ids = IDs.Count;
                    int records_number = (dataGridView1.Rows.Count - 1) - ids;

                    for (int y = 0; y < records_number; y++)
                    {
                        IDs.Add(dataGridView1[0, ids + y].Value.ToString());
                        a.AddNewRestaurantRecommendation(ref error, dataGridView1[0, ids + y].Value.ToString(), dataGridView1[1, ids + y].Value.ToString(), dataGridView1[2, ids + y].Value.ToString(), dataGridView1[3, ids + y].Value.ToString());
                    }
                }
                for (int j = 0; j < dataGridView1.Rows.Count - 1; j++)
                {

                    a.EditRestaurantRecommendation(ref error, IDs[j], dataGridView1[0, j].Value.ToString(), dataGridView1[1, j].Value.ToString(), dataGridView1[2, j].Value.ToString(), dataGridView1[3, j].Value.ToString());

                }
            }
            if (TABLESNAME[comboBox1.Text] == "sounds")
            {
                if (dataGridView1.Rows.Count - 1 > IDs.Count)
                {
                    int ids = IDs.Count; 
                    int records_number = (dataGridView1.Rows.Count - 1) - ids;

                    for (int y = 0; y < records_number; y++)
                    {
                        IDs.Add(dataGridView1[0, ids + y].Value.ToString());
                        a.AddNewSound(ref error, dataGridView1[0, ids + y].Value.ToString(), dataGridView1[1, ids + y].Value.ToString(), dataGridView1[2, ids + y].Value.ToString(), dataGridView1[3, ids + y].Value.ToString());

                    }
                }
                for (int j = 0; j < dataGridView1.Rows.Count - 1; j++)
                {

                    a.EditSound(ref error, IDs[j], dataGridView1[0, j].Value.ToString(), dataGridView1[1, j].Value.ToString(), dataGridView1[2, j].Value.ToString(), dataGridView1[3, j].Value.ToString());

                }
            }
            if (TABLESNAME[comboBox1.Text] == "transportation_recommendations")
            {
                if (dataGridView1.Rows.Count - 1 > IDs.Count)
                {
                    int ids = IDs.Count;
                    int records_number = (dataGridView1.Rows.Count - 1) - ids;

					for (int y = 0; y < records_number; y++)
					{
						IDs.Add(dataGridView1[0, ids + y].Value.ToString());
                        a.AddNewTransportationRecommendation(ref error, dataGridView1[0, ids + y].Value.ToString(), dataGridView1[1, ids + y].Value.ToString(), dataGridView1[2, ids + y].Value.ToString());
                    }
                }
                for (int j = 0; j < dataGridView1.Rows.Count - 1; j++)
                {

                    a.EditTransportationRecommendation(ref error, IDs[j], dataGridView1[0, j].Value.ToString(), dataGridView1[1, j].Value.ToString(), dataGridView1[2, j].Value.ToString());

                }
            }
            if (TABLESNAME[comboBox1.Text] == "wifipassword")
            {
                if (dataGridView1.Rows.Count - 1 > IDs.Count)
                {
                    int ids = IDs.Count;
                    int records_number = (dataGridView1.Rows.Count - 1) - ids;

                    for (int y = 0; y < records_number; y++)
                    {
                        IDs.Add(dataGridView1[0, ids + y].Value.ToString());
                        a.AddNewWifiPassword(ref error, dataGridView1[0, ids + y].Value.ToString(), dataGridView1[1, ids + y].Value.ToString());
                    }
                }
                for (int j = 0; j < dataGridView1.Rows.Count - 1; j++)
                {

                    a.EditWifiPassword(ref error, IDs[j], dataGridView1[0, j].Value.ToString(), dataGridView1[1, j].Value.ToString());

                }
            }
            if (TABLESNAME[comboBox1.Text] == "assistant_to_room_map")
            {
                if (dataGridView1.Rows.Count - 1 > IDs.Count)
                {
                    int ids = IDs.Count;
                    int records_number = (dataGridView1.Rows.Count - 1) - ids;

                    for (int y = 0; y < records_number; y++)
                    {
                        IDs.Add(dataGridView1[0, ids + y].Value.ToString());
                        a.AddNewAssistant(ref error, Convert.ToInt32(dataGridView1[0, ids+y].Value), dataGridView1[1, ids+y].Value.ToString());
                    }
                }
                for (int j = 0; j < dataGridView1.Rows.Count - 1; j++)
                {

                    a.EditAssistant(ref error, Convert.ToInt32(IDs[j]), Convert.ToInt32(dataGridView1[0, j].Value.ToString()), dataGridView1[1, j].Value.ToString());

                }
            }
            MessageBox.Show("succesfully updated!");
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            DBConnect a = new DBConnect( HotelInfo["DBUser"], HotelInfo["DBName"], HotelInfo["DBPassword"]);
            string error = " ";
            if (TABLESNAME[comboBox1.Text] == "guests")
            {
                int row_index = dataGridView1.CurrentCell.RowIndex;
                a.DeleteGuest(ref error, dataGridView1[0, row_index].Value.ToString());
                dataGridView1.Rows.RemoveAt(row_index);
            }
            if (TABLESNAME[comboBox1.Text] == "feedback_question")
            {
                int row_index = dataGridView1.CurrentCell.RowIndex;
                a.DeleteFeedbackQuestion(ref error, dataGridView1[0, row_index].Value.ToString());
                dataGridView1.Rows.RemoveAt(row_index);
            }
            if (TABLESNAME[comboBox1.Text] == "places_recommendations")
            {
                int row_index = dataGridView1.CurrentCell.RowIndex;
                a.DeletePlaceRecommendation(ref error, dataGridView1[0, row_index].Value.ToString());
                dataGridView1.Rows.RemoveAt(row_index);
            }
            if (TABLESNAME[comboBox1.Text] == "restaurants_recommendations")
            {
                int row_index = dataGridView1.CurrentCell.RowIndex;
                a.DeleteRestaurantRecommendation(ref error, dataGridView1[0, row_index].Value.ToString());
                dataGridView1.Rows.RemoveAt(row_index);
            }
            if (TABLESNAME[comboBox1.Text] == "sounds")
            {
                int row_index = dataGridView1.CurrentCell.RowIndex;
                a.DeleteSound(ref error, dataGridView1[0, row_index].Value.ToString());
                dataGridView1.Rows.RemoveAt(row_index);
            }
            if (TABLESNAME[comboBox1.Text] == "transportation_recommendations")
            {
                int row_index = dataGridView1.CurrentCell.RowIndex;
                a.DeleteSound(ref error, dataGridView1[0, row_index].Value.ToString());
                dataGridView1.Rows.RemoveAt(row_index);
            }
            if (TABLESNAME[comboBox1.Text] == "wifipassword")
            {
                int row_index = dataGridView1.CurrentCell.RowIndex;
                a.DeleteWifiPassword(ref error, dataGridView1[0, row_index].Value.ToString());
                dataGridView1.Rows.RemoveAt(row_index);
            }
            if (TABLESNAME[comboBox1.Text] == "assistant_to_room_map")
            {
                int row_index = dataGridView1.CurrentCell.RowIndex;
                a.DeleteAssistant(ref error, Convert.ToInt32(dataGridView1[0, row_index].Value.ToString()));
                dataGridView1.Rows.RemoveAt(row_index);
            }
            MessageBox.Show("succesfully Deleted!");
        }


        private void notify_btn_Click(object sender, EventArgs e)
        {
            register.Visible = true;
            checkin.Visible = true;
            tablespanel.Visible = true;
            notify_panel.Visible = true;
            this.Text = "Notifications";
            StreamReader sr = new System.IO.StreamReader("emergency.json");
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(sr.ReadToEnd());
            foreach (var dictionary in list)
                Console.WriteLine(dictionary["room"] + " pressed emergency in " + dictionary["datetime"]);
            dataGridView2.DataSource = list;
            sr.Close();
        }

        private void back_from_noti_Click(object sender, EventArgs e)
        {
			this.Text = ("check-in");
			notify_panel.Visible = false;
            tablespanel.Visible = false;
        }
        private void trybtn_Click(object sender, EventArgs e)
        {
            PopupNotifier popup = new PopupNotifier();
            popup.TitleText = "Emergency";
            popup.ContentText = "help room number 5!";
            popup.Popup();
            DateTime localDate = DateTime.Now;
            StreamReader sr = new System.IO.StreamReader("emergency.json");
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<dynamic>>(sr.ReadToEnd());

            sr.Close();
            Dictionary<string, dynamic> dict = new Dictionary<string, dynamic>();
            dict.Add("room", "8");
            dict.Add("datetime", localDate);
            dict.Add("notification content", "blabla");
            list.Add(dict);

            dataGridView2.DataSource = list;
            dataGridView2.Update();
            dataGridView2.Refresh();
            var ll = Newtonsoft.Json.JsonConvert.SerializeObject(list);

            StreamWriter sw = new StreamWriter("emergency.json");
            sw.Write(ll);
            sw.Close();



        }



    }
}
