namespace DesktopApp2
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rememberCB = new System.Windows.Forms.CheckBox();
            this.loginbtn = new System.Windows.Forms.Button();
            this.registerl = new System.Windows.Forms.CheckBox();
            this.loginl = new System.Windows.Forms.CheckBox();
            this.register = new System.Windows.Forms.Panel();
            this.registerbtnr = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.loginr = new System.Windows.Forms.CheckBox();
            this.registerr = new System.Windows.Forms.CheckBox();
            this.register.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(312, 115);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(275, 27);
            this.textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(312, 199);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(275, 27);
            this.textBox2.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(312, 285);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(275, 27);
            this.textBox3.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(210, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 19);
            this.label1.TabIndex = 5;
            this.label1.Text = "Hotel name";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(210, 207);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 19);
            this.label2.TabIndex = 6;
            this.label2.Text = "User name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(210, 293);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 19);
            this.label3.TabIndex = 7;
            this.label3.Text = "Password";
            // 
            // rememberCB
            // 
            this.rememberCB.AutoSize = true;
            this.rememberCB.Enabled = false;
            this.rememberCB.Location = new System.Drawing.Point(214, 400);
            this.rememberCB.Name = "rememberCB";
            this.rememberCB.Size = new System.Drawing.Size(135, 23);
            this.rememberCB.TabIndex = 9;
            this.rememberCB.Text = "remember me";
            this.rememberCB.UseVisualStyleBackColor = true;
            this.rememberCB.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // loginbtn
            // 
            this.loginbtn.AutoEllipsis = true;
            this.loginbtn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginbtn.Location = new System.Drawing.Point(414, 380);
            this.loginbtn.Name = "loginbtn";
            this.loginbtn.Size = new System.Drawing.Size(173, 55);
            this.loginbtn.TabIndex = 10;
            this.loginbtn.Text = "Login";
            this.loginbtn.UseVisualStyleBackColor = true;
            this.loginbtn.Click += new System.EventHandler(this.button3_Click);
            // 
            // registerl
            // 
            this.registerl.Appearance = System.Windows.Forms.Appearance.Button;
            this.registerl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registerl.Location = new System.Drawing.Point(1, 2);
            this.registerl.Name = "registerl";
            this.registerl.Size = new System.Drawing.Size(394, 78);
            this.registerl.TabIndex = 11;
            this.registerl.Text = "Register";
            this.registerl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.registerl.UseVisualStyleBackColor = true;
            this.registerl.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // loginl
            // 
            this.loginl.Appearance = System.Windows.Forms.Appearance.Button;
            this.loginl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginl.Location = new System.Drawing.Point(394, 2);
            this.loginl.Name = "loginl";
            this.loginl.Size = new System.Drawing.Size(401, 78);
            this.loginl.TabIndex = 12;
            this.loginl.Text = "Login";
            this.loginl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.loginl.UseVisualStyleBackColor = true;
            this.loginl.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // register
            // 
            this.register.Controls.Add(this.loginr);
            this.register.Controls.Add(this.registerr);
            this.register.Controls.Add(this.registerbtnr);
            this.register.Controls.Add(this.checkBox1);
            this.register.Controls.Add(this.textBox9);
            this.register.Controls.Add(this.textBox8);
            this.register.Controls.Add(this.textBox7);
            this.register.Controls.Add(this.textBox6);
            this.register.Controls.Add(this.textBox5);
            this.register.Controls.Add(this.textBox4);
            this.register.Controls.Add(this.textBox10);
            this.register.Controls.Add(this.textBox11);
            this.register.Controls.Add(this.label11);
            this.register.Controls.Add(this.label10);
            this.register.Controls.Add(this.label9);
            this.register.Controls.Add(this.label8);
            this.register.Controls.Add(this.label7);
            this.register.Controls.Add(this.label6);
            this.register.Controls.Add(this.label5);
            this.register.Controls.Add(this.label4);
            this.register.Controls.Add(this.label12);
            this.register.Controls.Add(this.label13);
            this.register.Dock = System.Windows.Forms.DockStyle.Fill;
            this.register.Location = new System.Drawing.Point(0, 0);
            this.register.Name = "register";
            this.register.Size = new System.Drawing.Size(800, 513);
            this.register.TabIndex = 13;
            // 
            // registerbtnr
            // 
            this.registerbtnr.Enabled = false;
            this.registerbtnr.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registerbtnr.Location = new System.Drawing.Point(342, 409);
            this.registerbtnr.Name = "registerbtnr";
            this.registerbtnr.Size = new System.Drawing.Size(169, 70);
            this.registerbtnr.TabIndex = 43;
            this.registerbtnr.Text = "Register";
            this.registerbtnr.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(38, 425);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(281, 23);
            this.checkBox1.TabIndex = 42;
            this.checkBox1.Text = "I have read and agree to the terms";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged_1);
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(167, 191);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(200, 27);
            this.textBox9.TabIndex = 41;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(167, 228);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(200, 27);
            this.textBox8.TabIndex = 40;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(167, 269);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(200, 27);
            this.textBox7.TabIndex = 39;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(167, 310);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(200, 27);
            this.textBox6.TabIndex = 38;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(546, 226);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(231, 27);
            this.textBox5.TabIndex = 37;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(546, 267);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(231, 27);
            this.textBox4.TabIndex = 36;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(546, 310);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(231, 27);
            this.textBox10.TabIndex = 35;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(546, 186);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(231, 27);
            this.textBox11.TabIndex = 34;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(424, 313);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 19);
            this.label11.TabIndex = 33;
            this.label11.Text = "port#";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(424, 275);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 19);
            this.label10.TabIndex = 32;
            this.label10.Text = "Database name";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(424, 234);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 19);
            this.label9.TabIndex = 31;
            this.label9.Text = "Password";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(424, 194);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 19);
            this.label8.TabIndex = 30;
            this.label8.Text = "User nme";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 318);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 19);
            this.label7.TabIndex = 29;
            this.label7.Text = "number of rooms";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 277);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 19);
            this.label6.TabIndex = 28;
            this.label6.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 19);
            this.label5.TabIndex = 27;
            this.label5.Text = "User name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 194);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 19);
            this.label4.TabIndex = 26;
            this.label4.Text = "Hotel name";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(424, 115);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(160, 19);
            this.label12.TabIndex = 25;
            this.label12.Text = "Database information";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 115);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 19);
            this.label13.TabIndex = 24;
            this.label13.Text = "Hotel information";
            // 
            // loginr
            // 
            this.loginr.Appearance = System.Windows.Forms.Appearance.Button;
            this.loginr.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginr.Location = new System.Drawing.Point(397, 3);
            this.loginr.Name = "loginr";
            this.loginr.Size = new System.Drawing.Size(401, 78);
            this.loginr.TabIndex = 45;
            this.loginr.Text = "Login";
            this.loginr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.loginr.UseVisualStyleBackColor = true;
            // 
            // registerr
            // 
            this.registerr.Appearance = System.Windows.Forms.Appearance.Button;
            this.registerr.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registerr.Location = new System.Drawing.Point(0, 3);
            this.registerr.Name = "registerr";
            this.registerr.Size = new System.Drawing.Size(398, 78);
            this.registerr.TabIndex = 44;
            this.registerr.Text = "Register";
            this.registerr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.registerr.UseVisualStyleBackColor = true;
            this.registerr.CheckedChanged += new System.EventHandler(this.registerr_CheckedChanged);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 513);
            this.Controls.Add(this.register);
            this.Controls.Add(this.loginl);
            this.Controls.Add(this.registerl);
            this.Controls.Add(this.loginbtn);
            this.Controls.Add(this.rememberCB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Name = "Login";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.register.ResumeLayout(false);
            this.register.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox rememberCB;
        private System.Windows.Forms.Button loginbtn;
        private System.Windows.Forms.CheckBox registerl;
        private System.Windows.Forms.CheckBox loginl;
        private System.Windows.Forms.Panel register;
        private System.Windows.Forms.Button registerbtnr;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox loginr;
        private System.Windows.Forms.CheckBox registerr;
    }
}

